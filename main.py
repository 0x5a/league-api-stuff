#!/usr/bin/python3
# Author 0x5a
import requests
import json

api_key = "YOUR-API-KEY"
na = "https://na1.api.riotgames.com"

headers = {
    "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
    "X-Riot-Token": api_key,
    "Accept-Language": "en,fr;q=0.5",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0"
}
Requests = {
        "sum_name":"/lol/summoner/v4/summoners/by-name/",
        "spectate":"/lol/spectator/v4/active-games/by-summoner/",
        "entries":"/lol/league/v4/entries/by-summoner/"
        }
def get_summoner_details(name):
    url = na + Requests["sum_name"] + name
    query = requests.get(url,headers=headers)
    if query.status_code != 200:
            print("Status Code: %s" % query.status_code)
            return
    data = json.loads(query.text)
    sumlvl = data["summonerLevel"]
    encrypted_id  = data["id"]
    print("Level %s" % sumlvl)
    live_game(encrypted_id)

def live_game(eid): 
    print("Checking if ingame....")
    url2   = na + Requests["spectate"] + eid
    query  = requests.get(url2,headers=headers)
    data = json.loads(query.text)
    if query.status_code != 200 :
            print("Currently not in a game")
            return
    #print(data)
    game_id = data["gameId"]
    participants = {}
    pars = data["participants"]
    for person in pars:
        participants[person['summonerName']] = []
        participants[person['summonerName']].append(person["teamId"])
        participants[person['summonerName']].append(person["summonerId"])
        participants[person['summonerName']].append(person["championId"])
    blue_team = []
    red_team = []
    for ppl in participants:
        if participants[ppl][0] == 100:
            blue_team.append(ppl)
        else:
            red_team.append(ppl)
    champ_mastery = {}
    for ppl in participants:
        champ_mastery[ppl] = champion_master(participants[ppl][1],participants[ppl][2])

    #for x in range(0,5): 
    #     print(blue_team[x].ljust(16) +"\t" + red_team[x].ljust(16))
    #print(champ_mastery)
     
    print("Blue Team".ljust(40) + "\t" + "Red Team".ljust(40))
    for x in range(0,5):
        print(more_info(participants[blue_team[x]][1]).ljust(36) +" " +  str(champ_mastery[blue_team[x]]) + "\t" + more_info(participants[red_team[x]][1]).ljust(36) + " " + str(champ_mastery[red_team[x]]))




def champion_master(eid,champid):
    url = na + "/lol/champion-mastery/v4/champion-masteries/by-summoner/%s/by-champion/%s" % (eid,champid)
    query = requests.get(url,headers=headers)
    data = json.loads(query.text)
    return int(data["championLevel"])



def more_info(eid):
    url = na + Requests["entries"] + eid
    query = requests.get(url,headers=headers)
    if query.status_code != 200:
        print(query.status_code)
        return 

    data = json.loads(query.text)
    name = data[0]["summonerName"].ljust(16)
    rank = data[0]['tier'] +" " +  data[0]['rank']
    rank = rank.ljust(12)
    winrate = str((data[0]['wins'] / (data[0]['losses'] + data[0]['wins'])) * 100)
    ret_str =  name + " " + rank + " " + winrate[:4] + "%"
    return ret_str




while 1:
    try:
        Sum = input("Summoner: ")
        get_summoner_details(Sum)
    except Exception as e:
        print(e)
    except KeyboardInterrupt:
        exit()
